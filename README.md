## Exercise Steps

1. Standup a local kubernetes cluster with docker desktop.
    - with nginx and linkerd installed
    - store the manifest used in a /.k8s folder
2.  build an api in golang, rust, or python that
    - takes your birthdate your first name and your last name as arguments and returns the hash.
    - can take the hash of the arguments and and return the initial arguments by comparing the hash
    - Write unit test for these two functions.
    - store the code for this in `/cmd`
3. Write the dockerfile for the api to containerize it store this in `/Dockerfile`
4. Write the kubernetes manifest for the backend api which will take traffic via linkerd that starts on nginx and store these in `/.k8s`.
5. Deploy this on your local kubernetes cluster and test that it works
6. Push the source code, Dockerfile, and manifest to the repo.
7. Write the `/.gitlab-ci.yml` file to run the unit test against your code, create a semantic version release, build the container, and push it to the repo, tagged appropriately that we can use to deploy and test with.
8. Push the `/.gitlab-ci.yml` to the repo and validate that your test work as expected. 
9. Open a PR with your changes and email us back letting us know you are ready for us to review.


Extra:
- How did you handle logging? Why?
- Do the unit tests pass? Why, Why not?
- What are some possible issues with this design? Would you deploy this in production? Why, Why not?
- How would you scale this to 10 requests/seccond? How about 100 requests/second?
- How could we improve this scenario?